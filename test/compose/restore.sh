#!/bin/bash

echo "Using RESTORE_FILE=${RESTORE_FILE}"

. "${COMPOSE_DIR}/clarin/.env"
#See https://www.postgresql.org/docs/9.3/libpq-pgpass.html
echo "${DB_HOST}:${DB_PORT}:${DB_NAME}:${DB_USER}:${DB_PASSWORD}" > .pgpass
(cd "${COMPOSE_DIR}/clarin" && FILENAME=/backups/${RESTORE_FILE} docker-compose -f docker-compose.yml -f docker-compose-restore.yml up --force-recreate restore)
rm .pgpass